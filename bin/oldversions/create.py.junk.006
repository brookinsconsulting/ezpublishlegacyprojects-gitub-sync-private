#!/usr/bin/python

#
# Imports
#

import argparse
import os
import subprocess
import requests
import urllib3
import json
import github
import git

from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3.exceptions import SNIMissingWarning
from requests.packages.urllib3.exceptions import InsecurePlatformWarning
from github import Github, GithubException
from github.GithubObject import NotSet
from git import Repo

#
# Program Arguments
#

parser = argparse.ArgumentParser( # name='import_project_resources.py', # allow_abbrev=True,
                                  description='Import projects.ez.no project resources (subversion repositories and downloads) into git repositories into github.com/ezpublishlegacyprojects.' )
#parser.add_argument('integers', metavar='N', type=int, nargs='+',
#                    help='an integer for the accumulator')
parser.add_argument('--token', type=str,
                    required=True,
                    help='the oauth or personal access token from git repository provider (Required. Default: none)')
parser.add_argument('--account', type=str,
                    required=False,
                    help='the name of the account from git repository provider (Not implemented or Required. Default: none)')
parser.add_argument('--base_path', type=str,
                    required=False,
                    argument_default='/home/brookins/ezecosystem/projects.mirror.ezecosystem.org/',
                    help='the full base path (Optional. Default: /home/brookins/ezecosystem/projects.mirror.ezecosystem.org/)')
parser.add_argument('--local', type=str,
                    required=False,
                    argument_default=True,
                    help='perform only local actions. Prevents pushing created repositories to git repository provider (Optional. Default: True)')
parser.add_argument('--log_only', type=str,
                    required=False,
                    argument_default=False,
                    help='logs subversion authors only. Prevents almost all other program action (Deprecated. Optional. Default: False)')
parser.add_argument('--local', type=str,
                    required=False,
                    argument_default=True,
                    help='perform only local actions. Prevents pushing created repositories to git repository provider (Optional. Default: True)')
parser.add_argument('--subversion_only', type=str,
                    required=False,
                    argument_default=True,
                    help='imports only subversion resources. Prevents creating download repositories (Optional. Default: True)')
parser.add_argument('--subversion_import', type=str,
                    required=False,
                    argument_default=False,
                    help='import subversion resources. Creates git repositories and imports subversion repository files (Takes Exessively Long. Optional. Default: False)')
parser.add_argument('--download_only', type=str,
                    required=False,
                    argument_default=False,
                    help='imports only download resources. Prevents creating subversion repositories (Optional. Default: True)')
parser.add_argument('--download_import', type=str,
                    required=False,
                    argument_default=False,
                    help='import subversion resources. Creates git repositories and imports subversion repository files (Takes Exessively Long. Optional. Default: False)')


args = parser.parse_args()

# Arguments into variables
account = args.account
token = args.token
base_path = args.base_path
local = args.local
log_only = args.log_only
subversion_only = args.subversion_only
subversion_import = args.subversion_import
download_import = args.download_import
download_only = args.download_only
delete_only = True
delete_empty_repos = True
debug_exit_print_subversion = False
debug_exit_print_downloads = False
debug_exit_print_directories = False

# Dispaly Control Switches
verbose = True
debug_level = 0

print(args.token)
quit()

# Disable SSL Related Warnings (We already know we have old tools)
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)
requests.packages.urllib3.disable_warnings(SNIMissingWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Calculated Path Items
path = base_path + 'doc/'
mirror_path = base_path + 'mirror/'
subversion_path = mirror_path + 'subversion/'
downloads_path = mirror_path + 'downloads/'
repositories_path = mirror_path + 'repositories/'

# Project Lists
directories = [ d for d in os.listdir( path ) if os.path.isdir( os.path.join( path, d ) ) ]
directories_count = sum( [ len( directories ) ] )
projects_with_subversion = []
projects_with_downloads = []
projects = []


#
# Excludes
#

# Project Subversion Repository Excludes
subversion_exclude = [ '_', '30_second_timeout_remedy', '_http_', 'share', 'social', 'xajax', 'var', 'user', 'cie', 'bc', 'ciet', 'cmsxf2b', 'download', 'extension', 'ezcommunityopml', 'ezecosystem', 'ezauthorize', 'ezcurlgateway', 'ezjscore', 'ezpedia', 'ezsdk', 'manuals', 'membership', 'batchtool', 'bdbashcompletion', 'ezdbug', 'opensearch', 'ezgpg', 'ezoe2', 'ezpublish_translation_fre_fr', 'ez_cache_manager', 'cossi_pxml', 'ezjscore2', 'membership2', 'owner2', 'redirect2', 'soextra', 'ezless', 'swark', 'swark_for_ez_publish_4', 'all2egooglesitemaps', 'cjw_newsletter', 'dappsocial', 'ezdevtools', 'ezrestdebugger', 'eztags', 'jvlogout', 'less_css', 'ngcomments', 'ngconnect', 'ngindexer', 'nl_cronjobs', 'nxc_captcha', 'nxc_extendedfilter', 'nxc_hotkeys', 'owsimpleoperator', 'owsortoperator', 'phpids', 'qhjsiniloader', 'qhnotifications', 'remoting', 'remoting', 'strip_except', 'swark', 'swark_for_ez_publish_4', 'textmate_bundle_for_ez_template_and_ini_files', 'unofficialhabla', 'updatesearchindex', 'wrap_operator', 'yubico_yubikey_otp_extension_for_ez_publish', 'ezfluxbb', 'ezplanet', 'eztags', 'eztidy', 'ezurlfilterchinese', 'nmcontentclass', 'objectrelationbrowse', 'types', 'content', 'membership__1' ]

# Project Download Excludes
exclude_downloads = [ '_', '30_second_timeout_remedy', '_http_', 'share', 'social', 'xajax', 'var', 'user', 'bc', 'download', 'extension', 'ezcommunityopml', 'ezecosystem', 'ezjscore', 'ezpedia', 'ezsdk', 'manuals', 'membership', 'batchtool', 'bdbashcompletion', 'ezdbug', 'opensearch', 'ezgpg', 'ezoe2', 'all2egooglesitemaps', 'eztags', 'ngcomments', 'ngconnect', 'ngindexer', 'nl_cronjobs', 'nxc_captcha', 'nxc_extendedfilter', 'nxc_hotkeys', 'remoting', 'textmate_bundle_for_ez_template_and_ini_files', 'updatesearchindex', 'types', 'content', 'membership__1' ]


#
# Function Definitions
#

# Calls to GitHub
def github_call(url, token, data):
    headers = {'Authorization' : 'token ' + token}
    result = requests.post(url, data=data, headers=headers)
    return result

# Connect to GitHub
def connect_to_github( token ):
    try:
        github = Github( token )
        user = gh.get_user()
        if verbose:
            print( "Connection to GitHub Established!" )
        return github
    except NameError:
        print( "Connection to GitHub Failed!" )
        return False

# Test if path contains git repository
def is_git_repo( path ):
    try:
        _ = git.Repo( path ).git_dir
        return True
    except git.exc.InvalidGitRepositoryError:
        return False

# Test if local git repository (from remote clone) is an empty repository
def is_repo_empty( url, name, repositories_path ):
     rpath = repositories_path + name
     Repo.clone_from( url, rpath )
     # bare = git.Repo( rpath ).working_tree_dir
     rpath_files = os.listdir( rpath )
     rpath_files.remove( '.git' )
     if not rpath_files:
         bare = True
     if verbose:
         print( "Remote Repository Empty: " + str( bare ) )
     return bare

# Delete all repositories
def delete_repositories( directories, delete_only ):
    if delete_only:
        for( project ) in directories[:7500]:
            print( "Deleting Repository: " + project )
            try:
                gh = connect_to_github( token )
                user = gh.get_user()
                repository = user.get_repo( name )
        
                if repository.delete() == None:
                    print( "Repository Removed" )
            except NameError:
                print "Removal Failed: " + project

    print( "Repositories removal completed" )
    quit()

# Process all subversion project repositories
def process_project_repositories( projects_with_subversion ):
    for ( project ) in projects_with_subversion[:7500]:
        name = str( project )
        repo_name = name 
        project_url = "http://projects.ez.no/" + name
        project_svn_url = "http://svn.projects.ez.no/" + name
        if "bc" not in name and name not in subversion_exclude and subversion_only:
            print( "Project: " + name )
            # print( "Project Authors: ")
            # os.system( 'svn log --quiet http://svn.projects.ez.no/' + name + ' | grep -E "r[0-9]+ \| .+ \|" | cut -d"|" -f2 | sed "s/ //g" | sort | uniq' )
            print( " " )
            if not log_only:
                try:
                    gh = connect_to_github( token )
                    user = gh.get_user()
                    repository = user.get_repo( name )
                    repo_path = subversion_path + '/' + name + '/'
                    try:
                        repository.name
                        if verbose:
                            print("Repository Exists: " + repository.name )
                            print("Skipping Repository Creation ..")
                        if os.path.exists( repo_path ):
                            if subversion_import:
                                os.chdir( subversion_path )
                                print( "Importing raw svn repository history" );
                                os.system( 'git svn clone --prefix='' --authors-file=/home/brookins/.svn2git/authors --trunk=/ ' + project_svn_url + ' --tags=/tags/ --branches=/branches/' )
                                os.chdir( repo_path )
                                os.system( 'pwd' )
                                os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + name + '.git' )
                                if not local:
                                    os.system( 'git push --all github' )
                            if is_repo_empty( repository.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                if repository.delete() == None:
                                    print( "Deleted empty repository: " + repo_name )
                            os.chdir( base_path )
                            print( " " )
                        else:
                            if subversion_import:
                                os.chdir( subversion_path )
                                print( "Importing raw svn repository history" );
                                os.system( 'git svn clone --prefix='' --authors-file=/home/brookins/.svn2git/authors --trunk=/ ' + project_svn_url + ' --tags=/tags/ --branches=/branches/' )
                                os.chdir( repo_path )
                                os.system( 'pwd' )
                                os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + name + '.git' )
                                if not local:
                                    os.system( 'git push --all github ' )
                            if is_repo_empty( repository.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                if repository.delete() == None:
                                    print( "Deleted empty repository: " + repo_name )
                            os.chdir( base_path )
                            print( " " )
                    except NameError:
                        pass #print "Well it seems the repository did not exist after all"
                #else:
                    #if verbose:
                    #    print("Repository Exists: " + name )
                    #    print("Skipping Repository Creation")
                except github.GithubException as e:
                    # print e, e.request, e.response
                    if "bc" not in name:
                        print( "GitHub Repository Missing: " + name )
                        print( "Creating repository ..." )

                        repository_created = user.create_repo( name, project_url, project_url, False, True, True, True ) 

                        if repository_created:
                            print("Created {0} successfully!" . format( repository_created.name ) )
                            if os.path.exists( repo_path ):
                                if subversion_import:
                                    os.chdir( subversion_path )
                                    print( "Importing raw svn repository history" );
                                    os.system( 'git svn clone --prefix='' --authors-file=/home/brookins/.svn2git/authors --trunk=/ ' + project_svn_url + ' --tags=/tags/ --branches=/branches/' )
                                    os.chdir( repo_path )
                                    os.system( 'pwd' )
                                    os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + name + '.git' )
                                    if not local:
                                        os.system( 'git push --all github ' )
                                if is_repo_empty( repository_created.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                    if repository_created.delete() == None:
                                        print( "Deleted empty repository: " + repo_name )
                                os.chdir( base_path )
                                print( " " )
                            else:
                                if subversion_import:
                                    os.chdir( subversion_path )
                                    print( "Importing raw svn repository history" );
                                    os.system( 'git svn clone --prefix='' --authors-file=/home/brookins/.svn2git/authors --trunk=/ ' + project_svn_url + ' --tags=/tags/ --branches=/branches/' )
                                    os.chdir( repo_path )
                                    os.system( 'pwd' )
                                    os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + name + '.git' )
                                    if not local:
                                        os.system( 'git push --all github ' )
                                if is_repo_empty( repository_created.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                    if repository_created.delete() == None:
                                        print( "Deleted empty repository: " + repo_name )
                                os.chdir( base_path )
                                print( " " )

            else:
                if not verbose:
                    print( "Skipping repository: " + name )

# Process all project downloads
def process_project_downloads( projects_with_downloads ):
    for ( project ) in projects_with_downloads[:0]:
    # for ( project ) in [ 'membership2' ]:
        name = str( project )
        repo_name = name + '_downloads'
        project_url = "http://projects.ez.no/" + name
        repo_path = downloads_path + repo_name + '/'
        if "bc" not in name and name not in exclude_downloads and download_only:
            print( "Project: " + name )
            print( "Repo Name: " + repo_name )
            print( " " )
            if not log_only:
                try:
                    gh = connect_to_github( token )
                    user = gh.get_user()
                    repository = user.get_repo( repo_name )

                    try:
                        repository.name
                        if verbose:
                            print("Repository Exists: " + repository.name )
                            print("Skipping Repository Creation ..")
                        if os.path.exists( repo_path ):
                            if download_import:
                                os.chdir( downloads_path )
                                print( "Importing raw downloads into repository" );
                                os.system( 'git init ' + repo_path )
                                os.chdir( path )
                                os.system( "find " + path + name + "/downloads -name '*.html' -exec cat {} \;| egrep -o 'href=\"(.*?)\/content\/download\/[0-9]+\/[0-9]+\/version\/[0-9]+\/file\/(.*?)\"' | sed 's/^href=\"..\/..\/..\///' | sed 's/^href=\"..\/..\///' | sed 's/\"//' | python -c \"import sys, urllib as ul; urls=ul.unquote( sys.stdin.read() ).decode('utf8').rstrip().split('\\n'); sys.stdout.write('rsync -va --backup --suffix=$(date +\"_%s\")'); [ sys.stdout.write(' \\\"" + path + "' + item_path + '\\\"') for item_path in urls ]; sys.stdout.write(' \\\"" + repo_path + "\\\"' ); \" | sh" )
                                os.chdir( repo_path )
                                os.system( "git add *" )
                                os.system( "git commit -m\"Added: Imported projects.ez.no project download archives, renamed to fit in same directory in an automated manner\" ." )
                                os.system( 'pwd' )
                                os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + repo_name + '.git' )
                                if not local:
                                    os.system( 'git push --all github' )
                            if is_repo_empty( repository.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                if repository.delete() == None:
                                    print( "Deleted empty repository: " + repo_name )
                            os.chdir( base_path )
                            print( " " )
                        else:
                            if download_import:
                                os.chdir( downloads_path )
                                print( "Importing raw downloads into repository" );
                                os.system( 'git init ' + repo_path )
                                os.chdir( path )
                                os.system( "find " + path + name + "/downloads -name '*.html' -exec cat {} \;| egrep -o 'href=\"(.*?)\/content\/download\/[0-9]+\/[0-9]+\/version\/[0-9]+\/file\/(.*?)\"' | sed 's/^href=\"..\/..\/..\///' | sed 's/^href=\"..\/..\///' | sed 's/\"//' | python -c \"import sys, urllib as ul; urls=ul.unquote( sys.stdin.read() ).decode('utf8').rstrip().split('\\n'); sys.stdout.write('rsync -va --backup --suffix=$(date +\"_%s\")'); [ sys.stdout.write(' \\\"" + path + "' + item_path + '\\\"') for item_path in urls ]; sys.stdout.write(' \\\"" + repo_path + "\\\"' ); \" | sh" )
                                os.chdir( repo_path )
                                os.system( "git add *" )
                                os.system( "git commit -m\"Added: Imported projects.ez.no project download archives, renamed to fit in same directory in an automated manner\" ." )
                                os.system( 'pwd' )
                                os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + repo_name + '.git' )
                                if not local:
                                    os.system( 'git push --all github ' )
                            if is_repo_empty( repository.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                if repository.delete() == None:
                                    print( "Deleted empty repository: " + repo_name )
                            os.chdir( base_path )
                            print( " " )
                    except NameError:
                        print "Repository did not exist: " + repo_name
                    #else:
                        #if verbose:
                        #    print("Repository Exists: " + name )
                        #    print("Skipping Repository Creation")
                except github.GithubException as e:
                    # print e, e.request, e.response
                    if "bc" not in name:
                        print( "GitHub Repository Missing: " + repo_name )
                        print( "Creating repository ..." )

                        repository_created = user.create_repo( repo_name, project_url, project_url, False, True, True, True ) 

                        if repository_created:
                            print("Created {0} successfully!" . format( repository_created.name ) )
                            if os.path.exists( repo_path ):
                                if download_import:
                                    os.chdir( downloads_path )
                                    print( "Importing raw downloads into repository" );
                                    os.system( 'git init ' + repo_path )
                                    os.chdir( path )
                                    os.system( "find " + path + name + "/downloads -name '*.html' -exec cat {} \;| egrep -o 'href=\"(.*?)\/content\/download\/[0-9]+\/[0-9]+\/version\/[0-9]+\/file\/(.*?)\"' | sed 's/^href=\"..\/..\/..\///' | sed 's/^href=\"..\/..\///' | sed 's/\"//' | python -c \"import sys, urllib as ul; urls=ul.unquote( sys.stdin.read() ).decode('utf8').rstrip().split('\\n'); sys.stdout.write('rsync -va --backup --suffix=$(date +\"_%s\")'); [ sys.stdout.write(' \\\"" + path + "' + item_path + '\\\"') for item_path in urls ]; sys.stdout.write(' \\\"" + repo_path + "\\\"' ); \" | sh" )
                                    os.chdir( repo_path )
                                    os.system( "git add *" )
                                    os.system( "git commit -m\"Added: Imported projects.ez.no project download archives, renamed to fit in same directory in an automated manner\" ." )
                                    os.system( 'pwd' )
                                    os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + name + '.git' )
                                    if not local:
                                        os.system( 'git push --all github ' )
                                if is_repo_empty( repository_created.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                    if repository_created.delete() == None:
                                        print( "Deleted empty repository: " + repo_name )
                                os.chdir( base_path )
                                print( " " )
                            else:
                                if download_import:
                                    os.chdir( downloads_path )
                                    print( "Importing raw downloads into repository" );
                                    os.system( 'git init ' + repo_path )
                                    os.chdir( path )
                                    os.system( "find " + path + name + "/downloads -name '*.html' -exec cat {} \;| egrep -o 'href=\"(.*?)\/content\/download\/[0-9]+\/[0-9]+\/version\/[0-9]+\/file\/(.*?)\"' | sed 's/^href=\"..\/..\/..\///' | sed 's/^href=\"..\/..\///' | sed 's/\"//' | python -c \"import sys, urllib as ul; urls=ul.unquote( sys.stdin.read() ).decode('utf8').rstrip().split('\\n'); sys.stdout.write('rsync -va --backup --suffix=$(date +\"_%s\")'); [ sys.stdout.write(' \\\"" + path + "' + item_path + '\\\"') for item_path in urls ]; sys.stdout.write(' \\\"" + repo_path + "\\\"' ); \" | sh" )
                                    os.chdir( repo_path )
                                    os.system( "git add *" )
                                    os.system( "git commit -m\"Added: Imported projects.ez.no project download archives, renamed to fit in same directory in an automated manner\" ." )
                                    os.system( 'pwd' )
                                    os.system( 'git remote add github git@github-as-ezpublishlegacyprojects:ezpublishlegacyprojects/' + name + '.git' )
                                    if not local:
                                        os.system( 'git push --all github ' )
                                if is_repo_empty( repository_created.clone_url, repo_name, repositories_path ) and delete_empty_repos:
                                    if repository_created.delete() == None:
                                        print( "Deleted empty repository: " + repo_name )
                                os.chdir( base_path )
                                print( " " )

        else:
            print( "Skipping repository: " + name )

# Display project names and exit
def debug_exit_print( projects ):
    print projects
    quit()

# Calculate subversion and download projects from directories list
def calculate_projects( directories ):
    for ( subdirname ) in sorted( directories ):
        # for subdirname in sorted( dirnames ):
        dirpath = os.path.join( path, subdirname )
        # print( dirpath )
        # print ( subdirname )
        if subdirname not in subversion_exclude and os.path.isfile( dirpath + '.html' ):
            projects.append( subdirname )
            if subdirname not in subversion_exclude and os.path.isdir( dirpath + '/subversion' ):
                # print( subdirname )
                projects_with_subversion.append( subdirname )
            else:
                if os.path.isdir( dirpath + '/downloads' ) and subdirname not in subversion_exclude and subdirname not in projects_with_subversion and "bc" not in subdirname:
                    projects_with_downloads.append( subdirname )

# Display detected project totals (initial program output)
def print_detection_totals( projects, projects_with_downloads, projects_with_subversion ):
    print("Detected total projects: " + str( sum( [ len( projects ) ] ) ) )
    print("Detected download projects: " + str( sum( [ len( projects_with_downloads ) ] ) ) )
    print("Detected subversion projects: " + str( sum( [ len( projects_with_subversion ) ] ) ) )

#
# Begin Main Program
#

# Calculate projects
calculate_projects( directories )

# Display project totals
print_detection_totals( projects, projects_with_downloads, projects_with_subversion )

# Rest of The Show / Program (Filter into functions)

# Debug : Display projects
if debug_exit_print_subversion:
    debug_exit_print( projects_with_subversion )
elif debug_exit_print_downloads:
    debug_exit_print( projects_with_downloads )
elif debug_exit_print_directories:
    debug_exit_print( directories )

# Remove all repositories
delete_repositories( directories, delete_only )

# Process all project repositories
process_project_repositories( projects_with_subversion )

# Process all project downloads
process_project_downloads( projects_with_downloads )

# fin
quit()
