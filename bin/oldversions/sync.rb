#!/usr/bin/env ruby
require "shellwords"
require 'github_api'

# Require(s) used only for debugging
# require 'pp'
# require 'json'

# Test script command line arguments
mode = ARGV[0]
specific_account = ARGV[1]
specific_repo = ARGV[2]

# Test for empty mode requirement and default to test
if mode == nil
    mode = "test"
    puts "Executing script in #{mode} mode\n"
end

# Test for empty specific_repo optional parameter and default to false
if specific_repo == nil
    specific_repo = false
else
    puts "Executing script for only one repository: #{specific_repo} mode\n"
end

# Test for empty specific_repo optional parameter and default to false
if specific_account == nil
    specific_account = "ezpublishlegacy"
else
    puts "Executing script for only one account: #{specific_account}\n"
end

# Test for specific account github credentials
if specific_account == "ezpublishlegacy"
    github_client_id = "e905f84f88c6885287bf"
    github_client_secret = "b0d0a8d197c8c3ca7282d081935bf1a3b3ff9ccb"
    github_oath_token = "0009e4816c9cc829bce024beaed8b7759ea02487"
elsif specific_account == "ezecosystem"
    github_client_id = "3085272cdc50c56220b7"
    github_client_secret = "eb47587b54ae7d0fef2b"
    github_oath_token = "dd7706ce2fe7ed488bf82d7628f9b0e4284300d6"
end

# Script private keys (sensative information): Do not share!
github_user_name = specific_account
github_request_item_limit = 2500
github_auto_pagination = true
mirror_directory_path = Shellwords.escape( "/home/brookins/#{specific_account}/github.#{specific_account}.com/mirror" )

# Script function: sync_repository
def sync_repository( message, mode, cd_repository_directory_and_check_for_upstream_remote_shell_command, sync_repository_with_upstream_shell_command )
      upstream_repository_remote_name = `#{cd_repository_directory_and_check_for_upstream_remote_shell_command}`
      if upstream_repository_remote_name == "upstream"
         if mode == "live"
              puts message
              system sync_repository_with_upstream_shell_command
              puts "\nCompleted! Merged upstream changes and pushed to mirror.\n"
         else
              puts message
              puts sync_repository_with_upstream_shell_command
         end
      end
end

# Start script main body

# Configure Github connection oath keys and settings
Github.configure do |c|
  c.client_id = github_client_id
  c.client_secret = github_client_secret
  c.oauth_token = github_oath_token
  c.per_page = github_request_item_limit
  c.auto_pagination = github_auto_pagination
end

# Connect to Github API
github = Github.new

# Disabled GitHub connection debug inspection
# puts github.inspect + "\n\n"
# exit


# Fetch Github user repositories and iterate over returned list of repositories
account_repositories = github.repos.list per_page: github_request_item_limit, type: "all"

# Display current repository count durring execution
puts "Total number of repositories in mirror: #{account_repositories.count}\n"

for repository in account_repositories
    repository_name = Shellwords.escape( repository.name )
    repository_directory_name = "#{mirror_directory_path}/#{repository_name}"

    if specific_repo == false || specific_repo != false && specific_repo == repository_name
        cd_return_shell_command = "cd -"
        #### mkdir_repository_directory_shell_command = "mkdir #{repository_directory_name};"
        cd_repository_directory_shell_command = "cd #{repository_directory_name};"
        cd_repository_directory_and_show_remotes_shell_command = "#{cd_repository_directory_shell_command} git remote -v"
        cd_repository_directory_and_check_for_upstream_remote_shell_command = "#{cd_repository_directory_shell_command} git remote -v|grep upstream|grep \\(fetch\\)| cut -f 1|tr -d '\n'"
        sync_repository_with_upstream_shell_command = "#{cd_repository_directory_shell_command} git checkout master; git fetch upstream --prune; git merge upstream/master; git push --all origin;"

        repository_ssh_url = repository.ssh_url
        repository_clone_ssh_url = repository_ssh_url.gsub("@github.com","@github-as-#{specific_account}")
        clone_shell_command = "git clone #{repository_clone_ssh_url} #{repository_directory_name}"
        ## clone_shell_command = "git clone --mirror #{repository_clone_ssh_url} #{repository_directory_name}"

        # Fetch repository specific information
        repository_info = github.repos.get user: github_user_name, repo: repository_name
        repository_is_a_fork = repository_info.fork
        repository_source = repository_info["source"]
        repository_parent = repository_info["parent"]

        if repository_is_a_fork == true
            repository_source_name = repository_source["name"]
            repository_source_owner = repository_source["owner"]
            repository_source_owner_user = repository_source_owner["login"]

            repository_parent_name = repository_parent["name"]
            repository_parent_owner = repository_parent["owner"]
            repository_parent_owner_user = repository_parent_owner["login"]

            if repository_parent_owner_user != repository_source_owner_user
                repository_remote_upstream_user_login = repository_parent_owner_user
                repository_remote_upstream_name = repository_parent_name
                if mode == "test"
                    puts "\nFork: repository_parent_owner_user != repository_source_owner_user\n"
                    puts "Fork repo username: " + repository_parent_owner_user
                    puts "Fork repo name: " + repository_parent_name
                    puts "Original repo author username: " + repository_source_owner_user
                    puts "Original repo name: " + repository_source_name
                end
            else
                if mode == "test"
                    puts "\nFork: repository_parent_owner_user == repository_source_owner_user\n"
                    puts "Fork repo username: " + repository_parent_owner_user
                    puts "Fork repo name: " + repository_parent_name
                    puts "Original repo author username: " + repository_source_owner_user
                    puts "Original repo name: " + repository_source_name
                end
                repository_remote_upstream_user_login = repository_source_owner_user
                repository_remote_upstream_name = repository_source_name
            end
        else
            repository_remote_upstream_name = repository_name
            repository_remote_upstream_user_login = github_user_name
        end

      clone_remote_shell_command = "git remote add upstream git@github-as-#{specific_account}:#{repository_remote_upstream_user_login}/#{repository_remote_upstream_name}.git"
        cd_repository_directory_and_add_upstream_remote_shell_command = "#{cd_repository_directory_shell_command}#{clone_remote_shell_command}"

        if repository_is_a_fork == true
            if !File.directory?( repository_directory_name )
                puts "\nDiscovered repository: #{repository_name} at #{repository_clone_ssh_url} from #{repository_remote_upstream_user_login}: Cloning repository ...\n"
                if mode == "test"
                    puts clone_shell_command
                    puts cd_repository_directory_shell_command
                    puts clone_remote_shell_command
                    puts cd_return_shell_command
                    puts "\n"
                elsif mode == "live"
                    system clone_shell_command
                    upstream_repository_remote_name = `#{cd_repository_directory_and_check_for_upstream_remote_shell_command}`

                    if upstream_repository_remote_name != "upstream"
                        puts "Adding upstream repository remote configuration to git clone.\n"
                        system cd_repository_directory_and_add_upstream_remote_shell_command
                        system cd_repository_directory_and_show_remotes_shell_command
                    end

                    sync_message = "\nPreparing to sync local #{repository_name} checkout with upstream sources.\n"
                    puts sync_repository( sync_message, mode, cd_repository_directory_and_check_for_upstream_remote_shell_command, sync_repository_with_upstream_shell_command )
                end
            else
                upstream_repository_remote_name = `#{cd_repository_directory_and_check_for_upstream_remote_shell_command}`
                # puts "#{cd_repository_directory_and_check_for_upstream_remote_shell_command}"

                if upstream_repository_remote_name != "upstream"
                    puts "Adding upstream repository remote configuration.\n"
                    system cd_repository_directory_and_add_upstream_remote_shell_command
                    system cd_repository_directory_and_show_remotes_shell_command
                end

                sync_message = "\nFound existing #{repository_name} repository clone. Preparing to merge from upstream sources.\n\n"
                sync_repository( sync_message, mode, cd_repository_directory_and_check_for_upstream_remote_shell_command, sync_repository_with_upstream_shell_command )
            end
        end
    end
end

# End of script
exit
